from django.urls import path, include
from .views import PartnersList

urlpatterns = [
    path('api/partners/', PartnersList.as_view()),
]
