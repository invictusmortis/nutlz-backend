from django.db import models

# Create your models here.


class Partner(models.Model):
    contact = models.CharField(max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    company = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField()

    def __str__(self):
        return self.contact
