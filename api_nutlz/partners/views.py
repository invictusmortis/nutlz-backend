from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Partner
from .serializers import PartnerSerializer


# Create your views here.
class PartnersList(APIView):

    def get(self, request):
        qs = Partner.objects.all()
        serializer = PartnerSerializer(qs, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PartnerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(request.data)
        else:
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
