from django.shortcuts import render, get_object_or_404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Bar, Delivery
from .serializers import BarSerializer, DeliverySerializer

# Create your views here.


class BarList(APIView):

    def get(self, request):
        qs = Bar.objects.all()
        qsd = Delivery.objects.all()
        bars = BarSerializer(qs, many=True)
        delivery = DeliverySerializer(
            qsd, many=True, context={"request": request})
        return Response({'bars': bars.data, 'delivery': delivery.data})
