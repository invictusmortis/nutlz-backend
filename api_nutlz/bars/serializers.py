from rest_framework import serializers
from .models import Bar, Package, Delivery, Ingredient


class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = ('value', 'price', 'thumbnail')


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('name',)


class BarSerializer(serializers.ModelSerializer):
    packages = PackageSerializer(many=True, read_only=True)
    nuts = IngredientSerializer(many=True, read_only=True)
    seeds = IngredientSerializer(many=True, read_only=True)
    others = IngredientSerializer(many=True, read_only=True)
    primary = IngredientSerializer(many=True, read_only=True)

    class Meta:
        model = Bar
        fields = '__all__'


class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = ('title', 'slug', 'price')
