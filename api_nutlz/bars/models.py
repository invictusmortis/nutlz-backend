from django.db import models

# Create your models here.


class Bar(models.Model):
    title = models.CharField(verbose_name='Название батончика', max_length=255)
    slug = models.SlugField(max_length=50)
    description = models.TextField(verbose_name='Описание')
    weight = models.PositiveIntegerField(verbose_name='Вес')
    expired = models.CharField(verbose_name='Годен', max_length=255)
    photo_front = models.ImageField(
        verbose_name='Фото передней стороны', upload_to='photos/%Y/%m/%d/', blank=True)
    photo_back = models.ImageField(
        verbose_name='Фото тыльной стороны', upload_to='photos/%Y/%m/%d/', blank=True)
    gif = models.ImageField(verbose_name='Гифка',
                            upload_to='gifs/%Y/%m/%d/', blank=True)
    nuts = models.ManyToManyField(
        'Ingredient', blank=True, verbose_name='Орехи', related_name='nuts')
    seeds = models.ManyToManyField(
        'Ingredient', blank=True, verbose_name='Семечки', related_name='seeds')
    others = models.ManyToManyField(
        'Ingredient', blank=True, verbose_name='Другие ингр.', related_name='others')
    primary = models.ManyToManyField(
        'Ingredient', verbose_name='Основные ингр.', related_name='primary')
    packages = models.ManyToManyField(
        'Package', verbose_name='Размеры коробок', related_name='+')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('id',)


class Package(models.Model):
    title = models.CharField(verbose_name='Название коробки', max_length=255)
    value = models.PositiveIntegerField(verbose_name='Количество в коробке')
    price = models.PositiveIntegerField(verbose_name='Цена за коробку')
    thumbnail = models.ImageField(
        verbose_name='Изображение коробки', upload_to='thumbnails/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('id',)


class Delivery(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название доставки')
    slug = models.CharField(max_length=255)
    price = models.PositiveIntegerField(verbose_name='Цена доставки')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('id',)


class Ingredient(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название игридиента')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id',)
