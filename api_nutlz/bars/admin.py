from django.contrib import admin
from .models import Bar, Package, Delivery, Ingredient

# Register your models here.

admin.site.register(Bar)
admin.site.register(Package)
admin.site.register(Delivery)
admin.site.register(Ingredient)
