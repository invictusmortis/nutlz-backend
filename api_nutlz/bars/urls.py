from django.urls import path, include
from .views import BarList

urlpatterns = [
    path('api/bars/', BarList.as_view()),
]
