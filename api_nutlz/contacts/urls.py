from django.urls import path, include
from .views import ContactInfoList

urlpatterns = [
    path('api/contactinfo/', ContactInfoList.as_view()),
]
