from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import ContactInfo
from .serializers import ContactInfoSerializer


# Create your views here.
class ContactInfoList(APIView):

    def get(self, request):
        contactInfo = ContactInfo.objects.all()
        serializer = ContactInfoSerializer(contactInfo, many=True)
        return Response(serializer.data)
