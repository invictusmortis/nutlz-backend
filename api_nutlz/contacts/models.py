from django.db import models

# Create your models here.


class ContactInfo(models.Model):
    title = models.CharField(max_length=255)
    iin = models.CharField(max_length=255)
    kpp = models.CharField(max_length=255)
    ogrn = models.CharField(max_length=255)
    bill = models.CharField(max_length=255)
    bank = models.CharField(max_length=255)
    bik = models.CharField(max_length=255)
    legal_address = models.TextField()
    phone = models.CharField(max_length=255)
    phone_link = models.CharField(max_length=255)
    mail = models.CharField(max_length=255)

    def __str__(self):
        return self.title
