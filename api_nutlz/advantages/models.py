from django.db import models

# Create your models here.


class Advantage(models.Model):
    title = models.CharField(
        max_length=255, verbose_name='Название преимущества')
    description = models.TextField(verbose_name='Описание преимущества')

    def __str__(self):
        return self.title
