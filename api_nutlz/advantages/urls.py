from django.urls import path, include
from .views import AdvantageList

urlpatterns = [
    path('api/advantages/', AdvantageList.as_view()),
]
