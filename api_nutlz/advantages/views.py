from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Advantage
from .serializers import AdvantageSerializer

# Create your views here.


class AdvantageList(APIView):

    def get(self, request):
        qs = Advantage.objects.all()
        serializer = AdvantageSerializer(qs, many=True)
        return Response(serializer.data)
