from rest_framework import serializers
from .models import Callme


class CallmeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Callme
        fields = '__all__'
