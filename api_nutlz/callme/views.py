from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Callme
from .serializers import CallmeSerializer


# Create your views here.
class CallmeList(APIView):

    def get(self, request):
        qs = Callme.objects.all()
        serializer = CallmeSerializer(qs, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CallmeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(request.data)
        else:
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
