from django.urls import path, include
from .views import CallmeList

urlpatterns = [
    path('api/callme/', CallmeList.as_view()),
]
