from django.db import models

# Create your models here.


class Callme(models.Model):
    contact = models.CharField(max_length=255)
    message = models.TextField()

    def __str__(self):
        return self.contact
