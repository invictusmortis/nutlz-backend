# -*- coding: utf-8 -*-
from django.core.wsgi import get_wsgi_application
import os
import sys
sys.path.insert(0, '/var/www/u0799520/data/www/api.nutlz.com/api_nutlz')
sys.path.insert(
    1, '/var/www/u0799520/data/mydjangoenv/lib/python3.7/site-packages')
os.environ['DJANGO_SETTINGS_MODULE'] = 'api_nutlz.settings'
application = get_wsgi_application()
